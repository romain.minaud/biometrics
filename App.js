import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { StyleSheet, Text, View, Button, Alert } from 'react-native';
import * as LocalAuthentication from 'expo-local-authentication';

export default class App extends Component {
    state = {
      compatible: false,
      fingerprints: false,
      result: '',
    };

    componentDidMount() {
      this.checkDeviceForHardware();
      this.checkForFingerprints();
    }
  
    checkDeviceForHardware = async () => {
      let compatible = await LocalAuthentication.hasHardwareAsync();
      this.setState({ compatible });
    };
  
    checkForFingerprints = async () => {
      let fingerprints = await LocalAuthentication.isEnrolledAsync();
      this.setState({ fingerprints });
    };
  
    scanFingerprint = async () => {
      let result = await LocalAuthentication.authenticateAsync(
        {
          promptMessage: "Coucou",
          cancelLabel: "Retour",
          fallbackLabel: "Fallback"
        }
      );
      console.log('Scan Result:', result);
      this.setState({
        result: JSON.stringify(result),
      });
    };

  
    render() {
      return (
        <View style={styles.container}>
          <Text style={styles.text}>
            Compatible Device? {this.state.compatible === true ? 'True' : 'False'}
          </Text>
          <Text style={styles.text}>
            Fingerprings Saved?{' '}
            {this.state.fingerprints === true ? 'True' : 'False'}
          </Text>
          <Text>{this.state.result}</Text>
          <Button
          onPress={() => this.scanFingerprint()}
          title="Trigger"
          color="#841584"
        />
        </View>
      );
    }
  }
  
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'space-around',
      backgroundColor: '#ecf0f1',
    },
    text: {
      fontSize: 18,
      textAlign: 'center',
    },
    button: {
      alignItems: 'center',
      justifyContent: 'center',
      width: 150,
      height: 60,
      backgroundColor: '#056ecf',
      borderRadius: 5,
    },
    buttonText: {
      fontSize: 30,
      color: '#fff',
    },
  });
  